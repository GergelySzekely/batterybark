#!/bin/bash

BARKSOUND=/usr/share/sounds/gnome/default/alerts/bark.ogg  

function nbark {
    pactl set-sink-volume 1 100%
    pactl set-sink-mute 1 0
    i=1
    while [ $i -le $1 ]
    do 
	cvlc  --play-and-exit $BARKSOUND
	i=$(( $i +1 ))
    done
}

function gbark {
	if [[ ` acpi | awk -F'[, ]' '{print ($3)}'` == "$1" ]] && [[ `acpi  | awk '{print ($4)-0}'` -ge "$2" ]] 
	then  
	    nbark $3
	fi
}

function lbark {
	if [[ ` acpi | awk -F'[, ]' '{print ($3)}'` == "$1" ]] && [[ `acpi  | awk '{print ($4)-0}'` -le "$2" ]]
	then  
	    nbark $3
	fi	
}

lbark Discharging 15 3

lbark Discharging 9 6

gbark Charging 96 1

gbark Full 100 2
